# Lodgebook-UI

This is the UI-only portion of the Logicbook application.

## Branch Status Trackers
##### Master
![master branch](https://codeship.com/projects/d2e89410-bd8e-0134-909d-26a40d05594e/status?branch=master)
##### Develop
![develop branch](https://codeship.com/projects/d2e89410-bd8e-0134-909d-26a40d05594e/status?branch=develop)

## Development

### Getting Started

##### Prerequisites
You will need NodeJS, BowerJS, and GruntJS installed to build the project. I personally use Webstorm, but any decent editor will work.

##### Installing dependencies
```bash
$ npm install
$ bower install
```

##### Starting local environment
```bash
$ grunt
```

1. Navigate to [http://localhost:3001/](http://localhost:3001/) to view front-end

##### Building for deployment
Please note that at this stage of the project it is unlikely you will need to use this command. Ideally this command should only be used by the server we deploy to.
```bash
$ grunt build
```
### Code Syntax
Note that this project is minified, so there's certain syntax you'll need to use in Angular to make sure you will encounter no errors.
##### For example, instead of
```javascript
app.config(function($stateProvider){
// code here
});
```

##### Use
```javascript
app.config(['$stateProvider', function($stateProvider) {
// code here
}]);
```

### Development Process
Please create a new fork and make changes on your fork only. When you are ready to merge changes, you may merge them to the develop branch.

### Testing
As of right now, we have no automated testing in place, but this will be useful for making sure our code is ready for production later down the line.