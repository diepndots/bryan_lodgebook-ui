module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt);
  var historyApiFallback = require('connect-history-api-fallback'); //required middleware for HTML5 mode on Angular

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    /*
     * https://browsersync.io
     * Use this whenever performing local
     * development. (NOTE) You may need to
     * add directories to watch here so that
     * browsersync automatically reloads
     */
    browserSync: {
      default: {
        files: {
          src: ['./dist/css/*.css', './dist/js/*.js', './dist/index.html', './dist/bower_components/**']
        },
        options: {
          watchTask: true,
          open: false,
          server: {
            baseDir: './dist',
            middleware: [ historyApiFallback() ]
          },
          port: 3001,
          scrollRestoreTechnique: 'cookie'
        }
      }
    },
    /*
     * http://sass-lang.com/
     * When adding .scss files, reference them
     * in the master.scss file (do not add it here)
     */
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'dist/css/custom.css': 'src/sass/master.scss'
        }
      }
    },
    /*
     * https://github.com/gruntjs/grunt-contrib-watch
     * Add files that need to be "compiled"
     * here. For example, SASS files or JS files
     * that need to be uglified
     */
    watch: {
      css: {
        files: ['./src/sass/*.scss'],
        tasks: ['sass']
      },
      js: {
        files: ['./src/app/components/**/*.js', './src/app/services/*.js', './src/app/components/shared/**/*.js', './src/app/app.js', './src/app/firebase.js', './src/app/routes.js', './src/app/directives/**/*.js', './src/app/filters/*.js'],
        tasks: ['uglify']
      },
      templates: {
        files: ['./src/app/**/**/*.tpl.html', './src/app/**/**/*.directive.html'],
        tasks: ['ngtemplates']
      },
      spec: {
        files: ['./dist/js/*.js', './spec/*.js'],
        tasks: ['karma:unit:start']
      }
    },
    /*
     * https://github.com/mishoo/UglifyJS
     * Concatenates files and minifies them.
     * Always add new JavaScript files that
     * are written specficially for this app here.
     * (NOTE) May not actually need to add things
     * manually anymore!!
     */
    uglify: {
      default: {
        // files: {
        //   './dist/js/angular-app.js': [
        //     './src/app/app.js',
        //     './src/app/routes.js'
        //   ]
        // },
        src: './src/app/**/*.js',
        dest: './dist/js/angular-app.js',
        options: {
          sourceMap: true,
          sourceMapName: './dist/js/angular-app.map',
          mangle: true
        }
      }
    },
    /*
     * https://www.npmjs.com/package/grunt-angular-templates
     * Concatenates templates and converts them to scripts
     * for identification from Angular. This should detect
     * any files with .tpl.html.
     */
    ngtemplates: {
      lodgebookApp: {
        options: {
          prefix: ''
        },
        cwd: './src/app/', // removes this part of the directory from the template URL
        src: './**/**/**.html',
        dest: './dist/js/angular-templates.js'
      }
    },
    /*
     *
     * Adds automatic Karma testing
     */
    karma: {
      unit: {
        configFile: 'karma.conf.js'
      }
    }
  });

  // Default task(s).
  grunt.registerTask('default', ['sass', 'uglify', 'ngtemplates', 'browserSync', 'watch']);

  // Default with Karma
  grunt.registerTask('with-tests', ['sass', 'uglify', 'ngtemplates', 'karma:unit:start', 'browserSync', 'watch']);

  // Task for deployment
  grunt.registerTask('deploy', ['sass', 'ngtemplates', 'uglify']);

};