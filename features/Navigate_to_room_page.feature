Feature: Navigate to room page from maid home page

  Scenario: Maid home page has rooms
    Given I am on the maid home page
    Then I should see hotel rooms
    When I click on a hotel room
    Then I should navigate to that hotel room page