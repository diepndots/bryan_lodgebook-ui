/**
 * Created by Christopher on 5/20/17.
 */

/*
 * This Unit test will be incomplete. Just doing it to learn Unit Testing :)
 * 
 */

describe('SERVICE User Service', function() {
  beforeEach(module('lodgebookApp'));

  var $userService, $localStorageService;

  beforeEach(inject(function(userService, localStorageService) {
    $userService = userService;
    $localStorageService = localStorageService;
  }));

  it('Sets User ID and Role in local storage', function() {
    $userService.setUserIdRole('IDXXX', 'NAMEXXX', 'ROLEXXX');
    expect($localStorageService.get('userId')).toEqual('IDXXX');
    expect($localStorageService.get('name')).toEqual('NAMEXXX');
    expect($localStorageService.get('role')).toEqual('ROLEXXX');
  });

  it('Sets Hotel ID in local storage', function() {
    $userService.setHotelId('HOTELXXX');
    expect($localStorageService.get('hotelId')).toEqual('HOTELXXX');
  });

});