describe('FILTER hasNoRoomsAssigned', function() {

  beforeEach(module('lodgebookApp'));

  var $filter;

  beforeEach(inject(function(_$filter_){
    $filter = _$filter_;
  }));

  it('returns empty array when given null', function() {
    var hasNoRoomsAssigned = $filter('hasNoRoomsAssigned');
    expect(hasNoRoomsAssigned(null)).toEqual([]);
  });

  it('returns only users with assigned rooms equal to 0', function() {
    var hasNoRoomsAssigned = $filter('hasNoRoomsAssigned');
    var userArray = [
      {
        "assigned_rooms_count" : 0,
        "name" : "Tyler Terrace",
        "role" : "maid"
      },
      {
        "assigned_rooms_count" : 0,
        "name" : "Jane Joiner",
        "role" : "maid"
      },
      {
        "name" : "Martha More",
        "role" : "maid-supervisor"
      },
      {
        "name" : "Donna Door",
        "role" : "manager"
      },
      {
        "assigned_rooms_count" : 2,
        "name" : "Liz Litmus",
        "role" : "maid"
      },
      {
        "assigned_rooms_count" : 7,
        "name" : "Paul Practical",
        "role" : "maid"
      }];

    expect(hasNoRoomsAssigned(userArray)).toEqual([
      {"assigned_rooms_count" : 0,
        "name" : "Tyler Terrace",
        "role" : "maid"
      },
      {
        "assigned_rooms_count" : 0,
        "name" : "Jane Joiner",
        "role" : "maid"
      }]);
  })

});