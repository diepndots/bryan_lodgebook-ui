'use strict';

(function() {
    var app = angular.module('lodgebookApp');

    app.controller('supervisorAssignController', ['$scope', '$stateParams', '$firebaseObject', '$firebaseArray','$window','$state','userService', 'roomService', function($scope, $stateParams,$firebaseObject, $firebaseArray,$window,$state,userService,roomService) {
        //if(userService.getRole() != 'maid-supervisor' && userService.getRole() != 'manager')
            //$state.transitionTo('view-no-permission');
        //$stateParams.maidID is passed by the supervisorHomeController. URL for supervisorAssignController is defined in routes.js
        var maidRef = firebase.database().ref("hotels/" + userService.getHotelId() + "/users/" + $stateParams.maidId);
        var maidObj = $firebaseObject(maidRef);

        var roomRef = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('room_number');
        var roomObj = $firebaseArray(roomRef);
        $scope.clicked = false;
        $scope.roomsSelected = [];
        $scope.numCheckout = 0; // Leaving here in case needed elsewhere
        $scope.numStayOver = 0; // Leaving here in case needed elsewhere
        $scope.numDirtyCheckout = 0;
        $scope.numDirtyStayover = 0;
        $scope.initial_length = 0;
        $scope.initial_rooms = [];

        /* TODO You can significantly reduce this code by using "ng-repeat filter as"
         * See example in home.tpl.html
         */
        roomObj.$loaded().then(function (rl) {

            var test_array = rl.slice(0);
            test_array.forEach(function (room) {
                if(room.assigned_to === $stateParams.maidId){
                    $scope.initial_rooms.push(room);
                    ++$scope.initial_length;
                }
               if(room.assigned_to != '' && room.assigned_to != $stateParams.maidId) {
                   rl.splice(rl.indexOf(room), 1);
               }
               // Leaving here in case needed elsewhere
               /*
               else if(room.clean_type === 'checkout')
                   ++$scope.numCheckout;
               else if(room.clean_type === 'stayover')
                   ++$scope.numStayOver;
               */
               else if(room.clean_type === 'checkout' && room.clean_status === 'dirty')
                   ++$scope.numDirtyCheckout;
               else if(room.clean_type === 'stayover' && room.clean_status === 'dirty')
                   ++$scope.numDirtyStayover;
            });

            $scope.roomList = rl;
        });

        maidObj.$loaded().then(function(maid) {
            $scope.maidName = maid.name;

        }).catch(function(e) {
            console.log(e);
        });


        $scope.selectRoom = function (r) {
            if ($scope.roomsSelected.indexOf(r)===-1)
                $scope.roomsSelected.push(r);
            else
                $scope.roomsSelected.splice($scope.roomsSelected.indexOf(r),1);
            if ($scope.initial_rooms.indexOf(r)===-1)
                $scope.initial_rooms.push(r);
            else
                $scope.initial_rooms.splice($scope.initial_rooms.indexOf(r),1);
 
        };
        $scope.sendRoom = function () {
          if($scope.roomsSelected.length === 0)
              $window.alert('Please select rooms to assign.');
          else {
              for(var i = 0; i<$scope.roomsSelected.length;++i){
                  var roomRef = firebase.database().ref('hotels/'+userService.getHotelId()+'/rooms/'+$scope.roomsSelected[i].$id);
                  var roomOb = $firebaseObject(roomRef);
                  roomOb.$loaded().then(function(room){

                      if(room.assigned_to != '') {
                          room.assigned_to = '';
                          room.assigned_maid_name = '';
                      }
                      else {
                          room.assigned_to = $stateParams.maidId;
                          room.assigned_maid_name = $scope.maidName;
                      }
                      room.$save().then(function(ref) {

                      });
                  });
              }
              roomService.recountAssignedRooms($stateParams.maidId);

            /* TODO @Samir for the sake of "good ux", never make the page change if the save is not successful
             *      The $state.transitionTo... code should be placed in the room.$save callback function above
             *      Always take advantage of your "success" and "error" callback functions/promises
             *      More likely than not, a success or error should affect the UI View so the user knows if
             *      something went right or wrong.
             */
              $state.transitionTo('view-supervisor-maid');
          }
        };

        $scope.is_assigned = function (room) {
          return room.assigned_to === $stateParams.maidId;
        };

    }]);
})();