'use strict';

(function () {
  var app = angular.module('lodgebookApp');

  app.controller('roomController', ['$scope', '$stateParams', '$firebaseObject', '$firebaseArray', 'userService', '$state', 'constantService','$rootScope', function ($scope, $stateParams, $firebaseObject, $firebaseArray, userService, $state, constantService,$rootScope) {

    $scope.roomId = '';
    $scope.value1 = true;
    $scope.tasks_checked = {};
    $scope.room_number = '';
    $scope.incomplete_tasks = [];

    $scope.maidId = $stateParams.assigned_to;  // MIGHT NEED TO REMOVE THIS LINE
    var roomRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + $stateParams.roomId);
    var roomObj = $firebaseObject(roomRef);

    $scope.room = roomObj;

    roomObj.$loaded().then(function (room) {
      $scope.room_number = room.room_number;
      $scope.assignedTo = room.assigned_to;
      $scope.maidName = room.assigned_maid_name;
      if (roomObj.incomplete_tasks != undefined)
        $scope.incomplete_tasks = roomObj.incomplete_tasks;
      var refTasks = firebase.database().ref('hotels/' + userService.getHotelId()).child('/taskSets').orderByChild('name').equalTo(room.clean_type.toUpperCase() + '_TASKS');
      var refTasksArray = $firebaseArray(refTasks);
      refTasksArray.$loaded().then(function (tasksArray) {
        $scope.tasksArray = tasksArray[0].tasks;
        $scope.populateTasksChecked();
        $scope.roomId = $stateParams.roomId;
      });

    });

    $scope.app_task = function (t) {
      if ($scope.incomplete_tasks.indexOf(t) === -1)
        $scope.incomplete_tasks.push(t);
      else
        $scope.incomplete_tasks.splice($scope.incomplete_tasks.indexOf(t), 1);
    };

    $scope.populateTasksChecked = function () {
      for (var i = 0; i < $scope.tasksArray.length; ++i) {
        if (roomObj.incomplete_tasks === undefined || roomObj.incomplete_tasks.indexOf($scope.tasksArray[i]) === -1) {
          $scope.tasks_checked[$scope.tasksArray[i]] = true;
        } else {
          $scope.tasks_checked[$scope.tasksArray[i]] = false;
        }
      }
    };


    $scope.push_inc_task = function () {
      roomObj.incomplete_tasks = $scope.incomplete_tasks;

      roomObj.$save().then(function (ref) {
        });
      $scope.updateRoom();
      };


    $scope.isSupervisor = function () {
      return userService.getRole() === 'maid-supervisor';
    };

    $scope.isMaid = function() {
      return userService.getRole() === 'maid';
    };

    $scope.updateRoom = function () {
      if (roomObj.incomplete_tasks.length != 0) {
        if (roomObj.incomplete_tasks.length === $scope.tasksArray.length) {
          roomObj.clean_status = "dirty";
          roomObj.$save().then(function (ref) {
          });
        } else {
          roomObj.clean_status = "incomplete";
          roomObj.$save().then(function (ref) {
          });
        }
      } else {
          if ($scope.isMaid()) {
            roomObj.clean_status = 'pending';
            roomObj.$save().then(function (ref) {
            });
          }
          else {
              roomObj.clean_status = 'clean';
              roomObj.$save().then(function (ref) {
              });
          }
      }

    };

    $scope.addComment = function () {
      // once successful, clear the comment input and close the comment input elements
      // TODO NOTE: This method is not safe - when adding authentication, a function must be made on the server side
      // to inject author ID and timestamp automatically. This can technically be altered on the client browser right now
      if (roomObj.comments) {
        console.log(roomObj.comments);
        roomObj.comments.push({author: $scope.maidId, timestamp: Date.now(), comment: $scope.commentInput});
      } else {
        roomObj.comments = [{author: $scope.maidId, timestamp: Date.now(), comment: $scope.commentInput}];
      }
      roomObj.$save().then(function (ref) {
        console.log('success')
      }, function (err) {
        console.log(err);
      });
    };

    $scope.changeCleanType = function(cleanType) {
      roomObj.clean_type = cleanType;
      roomObj.$save().then(function(ref) {

      });
    };


    $scope.inspectionPass = function () {
      roomObj.clean_status = 'clean';
      roomObj.$save().then(function (ref) {
        $state.transitionTo('view-rooms-to-inspect');
      });
    };

    $scope.inspectionFail = function () {
      roomObj.clean_status = 'incomplete';
      $scope.push_inc_task();
      roomObj.$save().then(function (ref) {
        $state.transitionTo('view-rooms-to-inspect');
      });
    };

    $scope.toggleDoNotDisturb = function (value) {
      roomObj.do_not_disturb = value;
      roomObj.$save().then(function (ref) {

      })
    };


    $scope.goBack = function() {
      $state.transitionTo($rootScope.prevStateName, $rootScope.prevParams? $rootScope.prevParams : {});
    };


    $scope.title = "this is a test task";
    $scope.description = "this is the description of the task";
    $scope.imageSrc = 'http://www.placehold.it/500x500';

  }]);

})();