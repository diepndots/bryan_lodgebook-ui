'use strict';

(function () {
  var app = angular.module('lodgebookApp');

  app.controller('navigationController', ['$scope', '$stateParams', 'userService', '$state', '$rootScope', function ($scope, $stateParams, userService, $state, $rootScope) {

    // TODO When real authentication is added, remove this hardcoded ID
    userService.setHotelId('-KdrDS_Cj-NV380EfJaF');

    // TODO Remove whichever one is the duplicate...
   if(userService.getRole()==='maid') {
       $scope.mi = userService.getUserId();
   }
   $scope.logout = function () {
      userService.logout();
      $scope.username = userService.getUserName();
      $state.transitionTo('view-login');
   };
   $rootScope.$watch('isMaid',function () {
        if($rootScope.isMaid)
            $scope.mi = userService.getUserId();
   });

   $rootScope.$watch('isLoggedIn', function() {
       if ($rootScope.isLoggedIn)
           $scope.username = userService.getUserName();
   });

  }]);

})();