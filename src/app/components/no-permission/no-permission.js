(function () {
    var app = angular.module('lodgebookApp');

    app.controller('noPermissionController', ['$scope', '$stateParams', '$state','userService', function ($scope, $stateParams, $state,userService) {
        //***********************************************************
        //Test variables to generate a list of assigned rooms faster

        //***********************************************************

        $scope.redirectHome = function () {
                console.log('hello');
                if (userService.getRole() === null)
                    $state.transitionTo('view-login');
                else if (userService.getRole() === 'maid')
                    $state.transitionTo('view-maid-home',{maidId: userService.getUserId()});
                else if (userService.getRole() === 'maid-supervisor')
                    $state.transitionTo('view-rooms-to-inspect');
                //else //if(userService.getRole() === 'manager')
                    //$state.transtionTo('manager-home')
            };



    }]);
})();