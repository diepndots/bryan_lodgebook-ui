'use strict';

(function () {
    var app = angular.module('lodgebookApp');

    app.controller('roomListController', ['$scope', '$firebaseObject', '$firebaseArray', '$location', '$stateParams','userService', function ($scope, $firebaseObject, $firebaseArray, $location,$stateParams,userService) {

        //************************DATABASE QUERIES******************************************
        var maidRef = firebase.database().ref("hotels/" + userService.getHotelId() + "/users/").orderByChild('role').equalTo('maid');
        var maidArrayObj = $firebaseArray(maidRef);
        maidArrayObj.$loaded().then(function(maidList) {
            $scope.maidList = maidList;
            $scope.maidNameList = [];
            for (var i = 0; i < $scope.maidList.length; i++) {
                $scope.maidNameList.push({title: $scope.maidList[i].name, id: maidList[i].$id,
                    roomCount: maidList[i].assigned_rooms_count, active: 0});
            }
        }).catch(function(e) {
            console.log(e);
        });

        var roomRef = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('room_number');
        var roomArrayObj = $firebaseArray(roomRef);
        roomArrayObj.$loaded().then(function (roomList) {
                $scope.roomList = roomList;
        }).catch(function (e) {
            console.log(e);
        });

        //*****************************FILTERING DATA********************************************
        $scope.roomStatusList = [
            {title: "Dirty", active: 0},
            {title: "Incomplete", active: 0},
            {title: "Do Not Disturb", active: 0},
            {title: "Pending", active: 0},
            {title: "Clean", active: 0}
        ];

        $scope.roomTypeList = [
            {title: "Checkout", active: 0},
            {title: "Stayover", active: 0}
        ];

        $scope.vacancyStatusList = [
            {title: "Vacant", active: 0},
            {title: "Occupied", active: 0}
        ];

        $scope.selectedFilters = [];

        $scope.filterTabs = [
            {title: 'Maid', active: 0},
            {title: 'Room Status', active: 0},
            {title: 'Room Type', active: 0},
            {title: 'Vacancy Status', active: 0}
        ];

        $scope.selectFilterTab = function(tab) {
            tab.active = !tab.active;
            if ($scope.selectedTab == tab) {
                $scope.selectedTab = undefined;
            }
            else if ($scope.selectedTab != undefined) {
                $scope.selectedTab.active = 0;
                $scope.selectedTab = tab;
            }
            else {
                $scope.selectedTab = tab;
            }
        };

        $scope.resetFilters = function() {
            $scope.selectedFilters = [];

            $scope.maidNameList.forEach(function(filter) {
                filter.active = 0;
            });
            $scope.roomStatusList.forEach(function(filter) {
                filter.active = 0;
            });
            $scope.roomTypeList.forEach(function(filter) {
                filter.active = 0;
            });
            $scope.vacancyStatusList.forEach(function(filter) {
                filter.active = 0;
            });
        };

        $scope.selectFilter = function(filter) {
            var index;

            filter.active = !filter.active;
            if (filter.active == 0) {
                index = $scope.selectedFilters.indexOf(filter);
                $scope.selectedFilters.splice(index,1);
            }
            else if (filter.active == 1) {
                $scope.selectedFilters.push(filter);
            }
        };
        //***************************GET ROOM COUNT******************************************
        $scope.getRoomCount = function(roomFilter) {
            if ($scope.filterTabs[0].active) {
                return roomFilter.roomCount;
            }
            else if ($scope.filterTabs[1].active) {
                if (roomFilter.title == "Do Not Disturb")
                    return $scope.roomList.filter(function(room) {return room.do_not_disturb}).length;
                return $scope.roomList.filter(function (room) {return room.clean_status == roomFilter.title.toLowerCase()}).length;
            }
            else if ($scope.filterTabs[2].active) {
                return $scope.roomList.filter(function (room) {return room.clean_type == roomFilter.title.toLowerCase()}).length;
            }
            else if ($scope.filterTabs[3].active) {
                return $scope.roomList.filter(function (room) {return room.vacancy_status == roomFilter.title.toLowerCase()}).length;
            }
        };

        //**********************COMPARATOR FUNCTIONS FOR FILTERING*******************8*************
        $scope.checkRoom = function(roomAttribute) {
            return $scope.selectedFilters.some(function(filter) {
                return roomAttribute == filter.title;
            });
        };
        $scope.checkActive = function(filterList) {
            for (var i = 0; i < filterList.length; i++) {
                if (filterList[i].active)
                    return true;
            }
            return false;
        };

        $scope.filterAllRooms = function(room) {
            if ($scope.selectedFilters.length == 0) {
                return true;
            }
            var isFiltered = true;
            if ($scope.checkActive($scope.maidNameList)) {
                isFiltered = isFiltered && $scope.selectedFilters.some(function(maidFilter) {
                    return maidFilter.id == room.assigned_to;
                });
            }
            if ($scope.checkActive($scope.roomStatusList)) {
                isFiltered = isFiltered && $scope.selectedFilters.some(function(statusFilter) {
                    if (statusFilter.title == "Do Not Disturb")
                        return room.do_not_disturb;
                    return statusFilter.title.toLowerCase() == room.clean_status;
                });
            }
            if ($scope.checkActive($scope.roomTypeList)) {
                isFiltered = isFiltered && $scope.selectedFilters.some(function(typeFilter) {
                    return typeFilter.title.toLowerCase() == room.clean_type;
                });
            }
            if ($scope.checkActive($scope.vacancyStatusList)) {
                isFiltered = isFiltered && $scope.selectedFilters.some(function(vacancyFilter) {
                    return vacancyFilter.title.toLowerCase() == room.vacancy_status;
                });
            }
            return isFiltered;
        };

        //********************CHANGE CLEAN TYPE********************************88
        $scope.changeCleanType = function (roomNumber, cleanType) {
            var ref = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + roomNumber.$id);
            var roomObj = $firebaseObject(ref);
            roomObj.$loaded().then(function (room) {
                room.clean_status = cleanType;
                room.$save().then(function (ref) {
                });

            });
        };
    }]);

})();