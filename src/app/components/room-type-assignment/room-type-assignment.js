'use strict';

(function () {
    var app = angular.module('lodgebookApp');

    app.controller('roomTypeAssignmentController', ['$scope', '$window', '$firebaseObject', '$firebaseArray', '$location', '$stateParams','userService', 'constantService','roomService','$state',
        function ($scope, $window, $firebaseObject, $firebaseArray, $location,$stateParams,userService, constantService, roomService, $state) {

        var ref = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('room_number');
        var refArray = $firebaseArray(ref);
        $scope.inspectionStatus = constantService.inspectionStatus;
        $scope.roomsSelected = [];
        $scope.roomTypes = ['stayover,checkout'];
        refArray.$loaded().then(function (roomList) {
            $scope.roomList = roomList;
            angular.forEach($scope.roomList, function(room) {
                room.clicked = false;
            });
        }).catch(function (e) {
            console.log(e);
        });

        //**************************FILTERING DATA*************************************************
        $scope.filterTabs = [
            {title: 'Room Type', active: 0},
            {title: 'Vacancy Status', active: 0}
        ];

        $scope.roomTypeList = [
            {title: "Checkout", active: 0},
            {title: "Stayover", active: 0}
        ];

        $scope.vacancyStatusList = [
            {title: "Vacant", active: 0},
            {title: "Occupied", active: 0}
        ];

        $scope.selectedFilters = [];

        //******************************SELECT/DESELECT FUNCTIONS*************************************
        $scope.selectRoom = function (room) {
            if ($scope.roomsSelected.indexOf(room) === -1) {
                room.clicked = true;
                $scope.roomsSelected.push(room);
            }
            else {
                room.clicked = false;
                $scope.roomsSelected.splice($scope.roomsSelected.indexOf(room), 1);
            }
        };

        $scope.selectAllRooms = function() {
            if ($scope.roomsSelected.length !== $scope.roomList.length) {
                $scope.roomsSelected = [];
                angular.forEach($scope.roomList, function (room) {
                    room.clicked = true;
                    $scope.roomsSelected.push(room);
                });
            }
        };

        $scope.deselectAllRooms = function() {
            if ($scope.roomsSelected.length !== 0) {
                $scope.roomsSelected = [];
                angular.forEach($scope.roomList, function (room) {
                    room.clicked = false;
                });
            }
        };

        //*******************SELECT FILTER FUNCTIONS***********************************
        $scope.selectFilterTab = function(tab) {
            tab.active = !tab.active;
            if ($scope.selectedTab == tab) {
                $scope.selectedTab = undefined;
            }
            else if ($scope.selectedTab != undefined) {
                $scope.selectedTab.active = 0;
                $scope.selectedTab = tab;
            }
            else {
                $scope.selectedTab = tab;
            }
        };

        $scope.resetFilters = function() {
            $scope.selectedFilters = [];

            $scope.roomTypeList.forEach(function(filter) {
                filter.active = 0;
            });
            $scope.vacancyStatusList.forEach(function(filter) {
                filter.active = 0;
            });
        };

        $scope.selectFilter = function(filter) {
            var index;

            filter.active = !filter.active;
            if (filter.active == 0) {
                index = $scope.selectedFilters.indexOf(filter);
                $scope.selectedFilters.splice(index,1);
            }
            else if (filter.active == 1) {
                $scope.selectedFilters.push(filter);
            }
        };

        //******************************FILTER FUNCTIONS*****************************
        $scope.getRoomCount = function(roomFilter) {
            if ($scope.filterTabs[0].active) {
                return $scope.roomList.filter(function (room) {return room.clean_type == roomFilter.title.toLowerCase()}).length;
            }
            else if ($scope.filterTabs[1].active) {
                return $scope.roomList.filter(function (room) {return room.vacancy_status == roomFilter.title.toLowerCase()}).length;
            }
        };

        $scope.checkActive = function(filterList) {
            for (var i = 0; i < filterList.length; i++) {
                if (filterList[i].active)
                    return true;
            }
            return false;
        };

        $scope.filterAllRooms = function(room) {
            if ($scope.selectedFilters.length == 0) {
                return true;
            }
            var isFiltered = true;
            if ($scope.checkActive($scope.roomTypeList)) {
                isFiltered = isFiltered && $scope.selectedFilters.some(function(typeFilter) {
                        return typeFilter.title.toLowerCase() == room.clean_type;
                    });
            }
            if ($scope.checkActive($scope.vacancyStatusList)) {
                isFiltered = isFiltered && $scope.selectedFilters.some(function(vacancyFilter) {
                        return vacancyFilter.title.toLowerCase() == room.vacancy_status;
                    });
            }
            return isFiltered;
        };

        //**********************CHANGE TO STAYOVER/CHECKOUT/VACANT FUNCTIONS********************

        $scope.changeToStayover = function () {
             $scope.roomsSelected.forEach(function (room) {
                var ref = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + room.$id);
                var rObj = $firebaseObject(ref);
                rObj.$loaded().then(function (r) {
                    r.clean_type = 'stayover';
                    r.clean_status = 'dirty';
                    r.vacancy_status = 'occupied';
                    r.incomplete_tasks = null;
                    r.$save().then(function(ref) {
                    });
                });
             });
             $state.reload()
        };

        $scope.changeToDoNotDisturb = function () {
            $scope.roomsSelected.forEach(function (room) {
                var ref = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + room.$id);
                var rObj = $firebaseObject(ref);
                rObj.$loaded().then(function (r) {
                    r.do_not_disturb = !r.do_not_disturb;

                    r.$save().then(function (ref) {
                    });
                });
            });
            $state.reload()
        };


        $scope.changeToCheckout = function () {
            $scope.roomsSelected.forEach(function (room) {
                var ref = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + room.$id);
                var rObj = $firebaseObject(ref);
                rObj.$loaded().then(function (r) {
                    r.clean_type = 'checkout';
                    r.clean_status = 'dirty';
                    r.vacancy_status = 'occupied';
                    r.incomplete_tasks = null;
                    r.$save().then(function(ref) {
                    });
                });
            });
            $state.reload()
        };

        $scope.changeToVacant = function () {
            $scope.roomsSelected.forEach(function (room) {
                var ref = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + room.$id);
                var rObj = $firebaseObject(ref);
                rObj.$loaded().then(function (r) {
                    r.vacancy_status = 'vacant';
                    r.$save();
                });
            });
            $state.reload()
        };

        $scope.resetRooms = function() {
            if ($window.confirm("Are you sure you want to perform this reset?")) {
                $scope.roomsSelected.forEach(function (room) {
                    var roomsRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms/' + room.$id);
                    var roomObj = $firebaseObject(roomsRef);
                    roomObj.$loaded().then(function (r) {
                        r.vacancy_status = null;
                        r.clean_status = null;
                        r.clean_type = null;
                        r.assigned_to = '';
                        r.assigned_maid_name = '';
                        r.do_not_disturb = false;
                        r.incomplete_tasks = null;
                        r.$save();
                    });
                });

                var maidsRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/users/');
                var maidsArray = $firebaseArray(maidsRef);
                maidsArray.$loaded().then(function(maidList) {
                    maidList.forEach(function(maid) {
                        if (maid.role == 'maid') {
                            console.log(maid);
                            roomService.recountAssignedRooms(maid.$id);
                        }
                    });
                });
                $state.reload()
            }
        };

    }]);
})();