'use strict';

(function () {
    var app = angular.module('lodgebookApp');
    app.controller('supervisorHomeController', ['$scope', '$stateParams', '$firebaseArray','userService',
        function ($scope, $stateParams, $firebaseArray,userService) {

    var ref = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms').orderByChild('/clean_status').equalTo('pending');
    var refArray = $firebaseArray(ref);

    var childChangedRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms');
    childChangedRef.on("child_changed", function(snapshot) {
        if (snapshot.val().clean_status == 'pending') {
            $scope.list.sort(function(room1, room2) {
                return room1.room_number > room2.room_number;
            });
        }
    });

    refArray.$loaded().then(function (roomList) {
      $scope.list = roomList.sort(function(room1, room2) {
          return room1.room_number > room2.room_number;
      });
    }).catch(function (e) {
      console.log(e);
    });

  }]);
})();