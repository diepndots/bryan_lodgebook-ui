'use strict';

(function () {
  var app = angular.module('lodgebookApp');

  app.controller('supervisorMaidController', ['$scope', '$stateParams', '$firebaseArray','userService','$state', function ($scope, $stateParams, $firebaseArray,userService,$state) {

   // //if(userService.getRole() != 'maid-supervisor' && userService.getRole() != 'manager')
      //$state.transitionTo('view-no-permission');

    //creating maid queries
    var maidsRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/users').orderByChild('role').equalTo('maid');
    var maidsObj = $firebaseArray(maidsRef);
    maidsObj.$loaded().then(function (maidsList) {
      $scope.house_maid_list = maidsList;
    }).catch(function (e) {
      console.log(e);
    });

    $scope.availableMaids = [];
    $scope.otherMaids = [];
    $scope.maidAssignedRoomsCounts = {};

    //$scope.keys = Object.keys;
    /*
     * This function listens for any scope changes to the maid list and
     * will count rooms per maid each time the maids are updated
     * (which really should only be once)
     */

    /*
     * This function listens for any changes to the rooms ref and will
     * update the maids accordingly
     */
    // var roomRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/rooms');
    // var rooms = $firebaseArray(roomRef);
    // roomRef.on('value', function(snapshot) {
    //   //console.log(snapshot.val());
    // });

    /*
     * Algorithm
     *
     * Get Maid list
     * Get Room list
     * Create Available Maids array
     * Create Other Maids array
     * EVENT on Rooms values change, count how many times a maid is assigned to a room.
     *  THEN Assign maids to an array based on count of rooms they are assigned
     *
     *  Weird issues with the code below if I try to clean it up more...
     *
     */

    // rooms.$loaded().then(function(roomsList) {
    //   runAssignedRoomsCount(roomsList);
    // });

    // function runAssignedRoomsCount(roomsList) {
    //   $scope.maidAssignedRoomsCounts = {};
    //
    //   angular.forEach(roomsList, function(room) {
    //     var assignedTo = room.assigned_to;
    //     var assignedToObj = $scope.maidAssignedRoomsCounts[assignedTo];
    //     if(assignedTo!==null && assignedTo !== '') {
    //       if(assignedToObj !== null && assignedToObj !== undefined) {
    //         $scope.maidAssignedRoomsCounts[assignedTo] += 1;
    //       } else {
    //         $scope.maidAssignedRoomsCounts[assignedTo] = 1;
    //       }
    //     }
    //   });
    //
    // }



  }]);
})();