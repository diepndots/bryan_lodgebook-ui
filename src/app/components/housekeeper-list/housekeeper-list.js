'use strict';

(function () {
    var app = angular.module('lodgebookApp');

    app.controller('housekeeperListController', ['$scope', '$firebaseObject', '$firebaseArray', '$location', '$stateParams','userService', function ($scope, $firebaseObject, $firebaseArray, $location,$stateParams,userService) {
        var ref = firebase.database().ref('hotels/' + userService.getHotelId()).child('/users').orderByChild('name');
        var refArray = $firebaseArray(ref);
        refArray.$loaded().then(function (maidList) {
            var testArray = maidList.slice(0);
            testArray.forEach(function(user){
                if(user.role != 'maid') {
                    maidList.splice(maidList.indexOf(user), 1);
                }
            });
            $scope.list = maidList;
        }).catch(function (e) {
            console.log(e);
        });

    }]);

})();