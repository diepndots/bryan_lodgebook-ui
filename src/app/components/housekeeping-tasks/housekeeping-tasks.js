'use strict';

(function () {
    var app = angular.module('lodgebookApp');

    app.controller('maidHomeController', ['$filter', '$scope', '$firebaseObject', '$firebaseArray', '$location', '$stateParams','userService', function ($filter, $scope, $firebaseObject, $firebaseArray, $location,$stateParams,userService) {

        //***********************************************************
        //Test variables to generate a list of assigned rooms faster

        //***********************************************************

        // This is a long ass query, we can split this up into multiple variables, but I won't do that now
        // This query in English: "Get me all room that are assigned to this userID in this hotel"
        var ref = null;

        if($stateParams.maidId) {
            ref = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('assigned_to').equalTo($stateParams.maidId);
        } else {
            ref = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('assigned_to').equalTo(userService.getUserId());
        }
        var refArray = $firebaseArray(ref); // Never ever ever create an array to house Firebase data - Use $firebaseArray instead
        // When the query finishes and returns data...
        refArray.$loaded().then(function (roomList) {
            // Attach the finished data set to our $scope
            $scope.list = roomList;

        }).catch(function (e) {

            console.log(e);

        });

        $scope.changeClean = function(r){
            var ref = firebase.database().ref('hotels/'+userService.getHotelId()+'/rooms/'+r.$id);
            var roomObj = $firebaseObject(ref);
            roomObj.$loaded().then(function(room){
                room.clean_status = 'dirty';
                room.$save().then(function(ref) {
                });

            });

        };

        //#############################################################################################################
        //TODO: this function generates errors in the beginning because the $scope.list has no finished retrieving all the data
        $scope.getNumRoomStatus = function(status) {
            if (status == "All Rooms")
                return $scope.list.length;
            else if (status == "Do Not Disturb")
                return $filter('filter')($scope.list, {do_not_disturb: true}).length;
            return $filter('filter')($scope.list, {clean_status: status}).length;
        };

        $scope.roomStatusList = ['All Rooms', 'Dirty', 'Incomplete', 'Do Not Disturb', 'Pending', 'Clean'];
        $scope.selected = "All Rooms";
        $scope.selectTab = function(status) {
            $scope.selected = status;
        };

    }]);

})();