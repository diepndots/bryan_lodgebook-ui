/**
 * Created by brydi on 10/16/2017.
 */
'use strict';

(function() {
    var app = angular.module('lodgebookApp');

    app.directive('filterNavigationBar', [function() {
        return {
            templateUrl: './directives/filter-navigation-bar/filter-navigation-bar.directive.html',
            require: 'ngModel',
            scope: {
                tabs: "=ngModel",
                selectFunction: "=callback",
                roomCount: "=callback2"
            },
            link: function(scope, element, attribute) {

            }
        };
    }]);
})();