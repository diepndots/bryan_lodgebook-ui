/**
 * Created by Christopher on 4/29/17.
 */
'use strict';

(function() {
  var app = angular.module('lodgebookApp');

  app.directive('developerNavigation', ['$state', function($state) {
    return {
      restrict: 'E',
      templateUrl: './directives/developer-navigation/developer-navigation.directive.html',
      scope: {
        athletes: '=?'
      },
      link: function (scope, element, attrs) {
        scope.states = $state.get();
      }
    };
  }]);
})();