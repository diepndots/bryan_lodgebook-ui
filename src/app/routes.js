/*
 * This file houses all the URL routes as well as
 * references to the templates and controllers the
 * URL will activate.
 */

'use strict';

(function() {

  // Declaring this part of the main app
  var app = angular.module('lodgebookApp');

  app.config(['$stateProvider', '$locationProvider', function($stateProvider, $locationProvider) {

    /* Relevant to html5Mode
     *
     * https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions#how-to-configure-your-server-to-work-with-html5mode
     *
     */

    $locationProvider.html5Mode(true);
    
    /*
     * NOTE that Template URLs have a prefix the directory from the src folder
     *
     * Style Notes:
     * - "name" key should be lowercase with dashes as separators
     * - "url" key should be lowercase with dashes as separators
     */

    /* TODO add some logic here that checks if the user is already logged in
     * If so, redirect the user to the appropriate home page.
     */
      $stateProvider.state({
          name: 'on-start'
      });

    $stateProvider.state({
      name: 'view-login',
      url: '/login',
      templateUrl: './components/login/login.tpl.html',
      controller: 'loginController'
    });

    $stateProvider.state({
      name: 'view-maid-home',
      url: '/maid-home/:maidId',
      templateUrl: './components/housekeeping-tasks/housekeeping-tasks.html',
      controller: 'maidHomeController'
    });
    
    $stateProvider.state({
      name: 'view-room',
      url: '/room/:roomId',
      templateUrl: './components/room-info/room-info.tpl.html',
      controller: 'roomController'
    });

    $stateProvider.state({
        name: 'view-rooms-to-inspect',
        url: '/inspections',
        templateUrl: './components/rooms-to-inspect/rooms-to-inspect.html',
        controller: 'supervisorHomeController',
        roles: ['maid-supervisor']
    });

    $stateProvider.state({
        name: 'view-supervisor-maid',
        url: '/supervisor-maid',
        templateUrl: './components/view-maids-for-assignment/view-maids-for-assignment.html',
        controller: 'supervisorMaidController',
        roles: ['maid-supervisor']
    });

    $stateProvider.state({
        name: 'view-supervisor-assignments',
        url: '/supervisor-assignments/:maidId',
        templateUrl: './components/room-assignment/room-assignment.tpl.html',
        controller: 'supervisorAssignController',
        roles: ['maid-supervisor']
    });
      $stateProvider.state({
          name: 'view-no-permission',
          url: '/no-permission',
          templateUrl: './components/no-permission/no-permission.tpl.html',
          controller: 'noPermissionController'
      });
      $stateProvider.state({
          name: 'view-housekeeper-list',
          url: '/housekeeper-list',
          templateUrl: './components/housekeeper-list/housekeeper-list.tpl.html',
          controller: 'housekeeperListController',
          roles: ['maid-supervisor']
      });
      $stateProvider.state({
          name: 'view-room-list',
          url: '/room-list',
          templateUrl: './components/room-list/room-list.tpl.html',
          controller: 'roomListController',
          roles: ['maid-supervisor']
      });
      $stateProvider.state({
          name: 'view-room-type-assignment',
          url: '/room-type-assignment',
          templateUrl: './components/room-type-assignment/room-type-assignment.tpl.html',
          controller: 'roomTypeAssignmentController',
          roles: ['maid-supervisor']
      });


  }]);

})();