/**
 *
 * This is the main AngularJS file.
 * Inject all dependencies here. No additional code should be added here besides dependencies
 * and run blocks.
 *
 */
'use strict';

(function() {

  // Inject dependencies here
  var app = angular.module('lodgebookApp', ['ui.router', 'ui.bootstrap', 'firebase', 'ngAnimate', 'LocalStorageModule']);

    app.run(['userService','$rootScope',function (userService,$rootScope) {
       $rootScope.isMaid = userService.getRole()==='maid';
       $rootScope.isSupervisor = userService.getRole()==='maid-supervisor';
       $rootScope.isLoggedIn = userService.getLogInStatus();

    }]);



  app.run(['$transitions','$trace','$rootScope', function($transitions, $trace,$rootScope) {

    $trace.enable('TRANSITION');
      $transitions.onStart({to:'on-start',from:'*'}, function(trans) {
          var $state = trans.router.stateService;
          var userService = trans.injector().get('userService');
          var currentUserRole = userService.getRole();
              if (userService.getLogInStatus()) {
                  if (currentUserRole === 'maid') {
                      return $state.target('view-maid-home', {maidId: userService.getUserId()});
                  } else if (currentUserRole === 'maid-supervisor') {
                      return $state.target('view-rooms-to-inspect');
                  }
              } else {
                  return $state.target('view-login');
              }

      });

    $transitions.onStart({}, function(trans) {
      var $state = trans.router.stateService;
      var userService = trans.injector().get('userService');

      var targetState = trans.targetState()._identifier;
      var targetStateRoles = trans.targetState().state().roles? trans.targetState().state().roles : [];
      var currentUserRole = userService.getRole();
          // If there are no roles on the targetState, then this will let the user continue
          if (targetStateRoles.length === 0) {
              return;
          }
          // If there are roles on the targetState, we need to check if the user has that role
          if (targetStateRoles.includes(currentUserRole)) {
              return;
          } else {
              return $state.target('view-no-permission');
          }

    });

    $transitions.onSuccess({},function (trans) {
            $rootScope.prevParams = trans.params('from');
            $rootScope.prevStateName = trans.$from().name;

    });

  }]);

  app.run(['$transitions', function($transitions) {

    $transitions.onSuccess({}, function(trans) {
      document.body.scrollTop = document.documentElement.scrollTop = 0;
    })

  }]);
    app.run(['$state', function($state) {

        $state.transitionTo('on-start');

    }]);

    // Database recovery script
    // app.run(['$firebaseObject','$firebaseArray','userService','constantService', function($firebaseObject,$firebaseArray,userService,constantService) {
    //
    //     var ref = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('room_number');
    //     var refArray = $firebaseArray(ref);
    //     var listNum = [];
    //     refArray.$loaded().then(function (roomList) {
    //         var count = 0;
    //         roomList.forEach(function (r,i) {
    //             if(listNum.includes(r.room_number)){
    //                 roomList.$remove(i);
    //                 roomList.$save();
    //             }else{
    //                 listNum.push(r.room_number);
    //             }
    //
    //
    //         })
    //     }).catch(function (e) {
    //         console.log(e);
    //     });
    //
    // }]);

    // app.run(['$state','$transitions', function($state,$transitions) {
 //
 //     var userService = trans.injector().get('userService');
 //      if(userService.getRole() === null)
 //            $state.transitionTo('view-login');
 //        if (userService.getRole() === 'maid')
 //            $state.transitionTo('view-maid-home',{maidId: userService.getUserId()});
 //        else if (userService.getRole() === 'maid-supervisor')
 //            $state.transitionTo('view-rooms-to-inspect');
 //
 //
 //  }]);




})();