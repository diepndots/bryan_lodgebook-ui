'use strict';

(function() {

    var app = angular.module('lodgebookApp');

    app.service('userService', ['localStorageService','$rootScope', function(localStorageService,$rootScope) {


        this.setUserIdRole = function(userId,name,role) {
            localStorageService.set('userId',userId);
            localStorageService.set('name',name);
            localStorageService.set('role',role);
            localStorageService.set('isLoggedIn',true);
            $rootScope.isMaid = this.getRole()==='maid';
            $rootScope.isSupervisor = this.getRole()==='maid-supervisor';
            $rootScope.isLoggedIn = this.getLogInStatus();
        };

        this.setHotelId = function (hotelId) {
            localStorageService.set('hotelId',hotelId);
        };

        this.getHotelId = function () {
            return localStorageService.get('hotelId');
        };

        this.getUserId = function() {
            return localStorageService.get('userId');
        };

        this.getUserName = function() {
            return localStorageService.get('name');
        };

        this.getRole = function() {
            return localStorageService.get('role');
        };

        this.logout = function() {
            localStorageService.remove('userId','name','role');
            localStorageService.set('isLoggedIn',false);
            $rootScope.isMaid = this.getRole()==='maid';
            $rootScope.isSupervisor = this.getRole()==='maid-supervisor';
            $rootScope.isLoggedIn = this.getLogInStatus();
        };

        this.getLogInStatus = function () {
            return localStorageService.get('isLoggedIn');
        };



        return this;

    }]);


})();