'use strict';

(function() {

  var app = angular.module('lodgebookApp');

  app.service('constantService', [function() {

    this.inspectionStatus = {
      pending: 'INSPECTION_PENDING',
      failed: 'INSPECTION_FAILED',
      passed: 'INSPECTION_PASSED'
    };

    this.cleanStatus = {
      incomplete: 'CLEAN_STATUS_INCOMPLETE',
      dirty: 'CLEAN_STATUS_DIRTY',
      clean: 'CLEAN_STATUS_CLEAN'
    };

    this.cleanType = {

    };

    this.tasks = {
      checkoutTasks: 'CHECKOUT_TASKS',
      stayoverTasks: 'STAYOVER_TASKS'
    };

    return this;

  }]);


})();