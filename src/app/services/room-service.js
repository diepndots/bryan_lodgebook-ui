'use strict';

(function() {

    var app = angular.module('lodgebookApp');

    app.service('roomService', ['$firebaseObject', '$firebaseArray', 'userService', function($firebaseObject, $firebaseArray, userService) {

        this.recountAssignedRooms = function(maidId) {
            if (maidId) {
                var maidRef = firebase.database().ref('hotels/' + userService.getHotelId() + '/users/' + maidId);
                var maidObj = $firebaseObject(maidRef);
                var roomRef = firebase.database().ref('hotels/' + userService.getHotelId()).child('/rooms').orderByChild('room_number');
                var roomObj = $firebaseArray(roomRef);

                var room_count = 0;
                roomObj.$loaded().then(function (roomList) {
                    roomList.forEach(function (room) {
                        if (room.assigned_to == maidId) {
                            room_count++;
                        }
                    });
                    console.log(room_count + " rooms assigned for " + maidId);
                    maidObj.$loaded().then(function (maid) {
                        maid.assigned_rooms_count = room_count;
                        maid.$save();
                    });
                });
            }
        };

    }]);
})();